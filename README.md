Test scenes for my [Rust][rust] implementation of [PBRT][pbrt]
(Physically Based Rendering) - [rs_pbrt][rs_pbrt] (on **GitHub**).
See Wiki ...

[rust]:    https://www.rust-lang.org
[pbrt]:    http://www.pbrt.org
[rs_pbrt]: https://github.com/wahn/rs_pbrt
